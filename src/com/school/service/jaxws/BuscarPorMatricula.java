
package com.school.service.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class was generated by Apache CXF 2.7.18
 * Tue Jun 06 18:15:32 BRT 2017
 * Generated source version: 2.7.18
 */

@XmlRootElement(name = "buscarPorMatricula", namespace = "http://service.school.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "buscarPorMatricula", namespace = "http://service.school.com/")

public class BuscarPorMatricula {

    @XmlElement(name = "arg0")
    private int arg0;

    public int getArg0() {
        return this.arg0;
    }

    public void setArg0(int newArg0)  {
        this.arg0 = newArg0;
    }

}

