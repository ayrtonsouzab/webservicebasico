
package com.school.service.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class was generated by Apache CXF 2.7.18
 * Tue Jun 06 18:15:32 BRT 2017
 * Generated source version: 2.7.18
 */

@XmlRootElement(name = "buscarPorMatriculaResponse", namespace = "http://service.school.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "buscarPorMatriculaResponse", namespace = "http://service.school.com/")

public class BuscarPorMatriculaResponse {

    @XmlElement(name = "return")
    private com.school.service.Aluno _return;

    public com.school.service.Aluno getReturn() {
        return this._return;
    }

    public void setReturn(com.school.service.Aluno new_return)  {
        this._return = new_return;
    }

}

