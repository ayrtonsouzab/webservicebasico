package com.school.service;

import java.util.List;

public class Aluno {
	
	private int matricula;
	private String nome;
	private String email;
	private String telefone;
	private String curso;
	private List<Disciplina> disciplinas;
	
	public Aluno(){
		
	}
	
	public Aluno(int matricula,String nome, String email, String telefone, String curso, List<Disciplina> disciplinas) {
		super();
		this.matricula = matricula;
		this.nome = nome;
		this.email = email;
		this.telefone = telefone;
		this.curso = curso;
		this.disciplinas = disciplinas;
	}
	
	
	public int getMatricula() {
		return matricula;
	}

	public void setMatricula(int matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}

	public List<Disciplina> getDisciplinas() {
		return disciplinas;
	}

	public void setDisciplinas(List<Disciplina> disciplinas) {
		this.disciplinas = disciplinas;
	}
	
	
}
