package com.school.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(name = "SchoolService", targetNamespace = "http://service.school.com/")
public interface SchoolService {

	@WebMethod(operationName = "buscarPorMatricula", action = "urn:BuscarPorMatricula")
	Aluno buscarPorMatricula(@WebParam(name = "arg0") int matricula) throws Exception;

	@WebMethod(operationName = "disciplinasPorAluno", action = "urn:DisciplinasPorAluno")
	List<Disciplina> disciplinasPorAluno(@WebParam(name = "arg0") int matricula) throws Exception;

	@WebMethod(operationName = "mediaAluno", action = "urn:MediaAluno")
	double mediaAluno(@WebParam(name = "arg0") int matricula) throws Exception;

}