package com.school.service;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;

@WebService(targetNamespace = "http://service.school.com/", endpointInterface = "com.school.service.SchoolService", portName = "AlunoServiceImplPort", serviceName = "AlunoServiceImplService")
public class AlunoServiceImpl implements SchoolService {
	
	private static List<Aluno> alunos;
	
	public AlunoServiceImpl() {
		inicializarAlunos();
	}

	public Aluno buscarPorMatricula(int matricula) throws Exception{
		for(Aluno aluno:alunos){
			if(aluno.getMatricula() == matricula)
				return aluno;
		}
		throw new Exception("Aluno n�o encontrado pela matricula "+ matricula);
		
	}
	
	public List<Disciplina> disciplinasPorAluno(int matricula)throws Exception{
		List<Disciplina> disciplinas = new ArrayList<Disciplina>();
		for(Aluno aluno:alunos){
			if(aluno.getMatricula()==matricula){
				for(Disciplina disciplina: aluno.getDisciplinas()){
					disciplinas.add(disciplina);
				}
				return disciplinas;
			}
		}
		throw new Exception("N�o encontrado disciplinas para a matricula "+ matricula);
	}
	
	public double mediaAluno(int matricula)throws Exception{
		double notaTotal = 0;
		for(Aluno aluno:alunos){
			if(aluno.getMatricula()==matricula){
				for(Disciplina disciplina: aluno.getDisciplinas()){
					notaTotal = notaTotal + disciplina.getNota();
				}
				return notaTotal/aluno.getDisciplinas().size();
			}
		}
		throw new Exception("N�o encontrado media para a matricula"+ matricula);
	}
	
	private void inicializarAlunos() {
		alunos = new ArrayList<Aluno>();
		ArrayList<Disciplina> disciplinas1 = new ArrayList<>();
		disciplinas1.add(new Disciplina("Matematica",2.0));
		disciplinas1.add(new Disciplina("Portugues",4.0));
		alunos.add(new Aluno(1,"Joao","joao@joao.com","85984139766","TI",disciplinas1));
	
		//-------Aluno 2---------------------------------------------------------------------------
		ArrayList<Disciplina> disciplinas2 = new ArrayList<>();
		disciplinas2.add(new Disciplina("Matematica",2.5));
		disciplinas2.add(new Disciplina("Portugues",7.0));
		alunos.add(new Aluno(2,"Marcos","marcos@marcos.com","8532749405","Artes",disciplinas2));
	
		//------Aluno 3------------------------------------------------------------------------------	
		ArrayList<Disciplina> disciplinas3 = new ArrayList<>();
		disciplinas3.add(new Disciplina("Matematica",8.0));
		disciplinas3.add(new Disciplina("Portugues",3.0));
		alunos.add(new Aluno(3,"Andre","andre@andre.com","8899169716","Engenharia",disciplinas2));
	}
	
	
	
}
